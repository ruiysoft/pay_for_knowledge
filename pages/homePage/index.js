var base64 = require("../images/base64");
//index.js
//获取应用实例
const app = getApp();
var sliderWidth = 96; // 需要设置slider的宽度，用于计算中间位置

Page({
  data: {
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),

    imgUrls: null,

    tabs: ["音频", "视频", "图书"],
    activeIndex: 0,
    sliderOffset: 0,
    sliderLeft: 0,

    courseList: [],
    courseList2: [],
    courseList3: [],
  },

  onShow(e) {
    console.log(e);
    // this.onLoad();
  },

  onLoad: function () {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          sliderLeft: (res.windowWidth / that.data.tabs.length - sliderWidth) / 2,
          sliderOffset: res.windowWidth / that.data.tabs.length * that.data.activeIndex
        });
      }
    });

    // 轮播图
    wx.request({
      url: app.globalData.serverUrl + '/swiper/query',
      success(resp) {
        if(resp.data.success) {
          that.setData({
            imgUrls: resp.data.data
          });
        }
      }
    });

    wx.request({
      url: app.globalData.serverUrl + '/course/queryByCourseType',
      data: { courseType: 'courseType_01' },
      success(resp) {
        if (resp.data.success) {
          that.setData({
            courseList: resp.data.data
          });
        }
      }
    });

    wx.request({
      url: app.globalData.serverUrl + '/course/queryByCourseType',
      data: { courseType: 'courseType_02' },
      success(resp) {
        if (resp.data.success) {
          that.setData({
            courseList2: resp.data.data
          });
        }
      }
    });

    wx.request({
      url: app.globalData.serverUrl + '/course/queryByCourseType',
      data: { courseType: 'courseType_03' },
      success(resp) {
        if (resp.data.success) {
          that.setData({
            courseList3: resp.data.data
          });
        }
      }
    });

  },

  swiperClick(e) {
    console.log(e);
    console.log(e.target.dataset.routeurl);
    
    wx.navigateTo({
      url: e.target.dataset.routeurl,
    })
  },

  tabClick: function (e) {
    this.setData({
      sliderOffset: e.currentTarget.offsetLeft,
      activeIndex: e.currentTarget.id
    });
  },

  onShareAppMessage: function () {
    return {
      title: '给你知识',
      imageUrl: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1535167640343&di=a5a4920f77f87e57d4461b7dc16f40c8&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fimage%2Fc0%253Dshijue1%252C0%252C0%252C294%252C40%2Fsign%3D038f31b2ae0f4bfb98dd96176b261285%2Ffaedab64034f78f076bbc4de73310a55b3191c6e.jpg',
      path: '/pages/index/index'
    }
  },

})
