// pages/orderDetail/index.js
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    order: null,
    orderDetailList: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;

    this.setData({
      order: JSON.parse(options.order)
    });
    console.log(this.data.order);

    wx.request({
      url: app.globalData.serverUrl + '/order/queryDetailByOrderId',
      data: {
        id: this.data.order.id,
      },
      success(resp) {
        if (resp.data.success) {
          that.setData({
            orderDetailList: resp.data.data
          });
        }
      },
      fail(e) {
        wx.showToast({
          icon: 'none',
          title: e.errMsg,
        });
      }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  showPayDlg(orderNu) {
    var that = this;
    var total_fee = this.data.order.sumPrice;
    total_fee = (parseFloat(total_fee) * 100).toFixed();
    console.log(total_fee, total_fee);

    wx.request({
      url: app.globalData.serverUrl + "/payment/getPaySign",
      data: {
        body: this.data.orderDetailList[0].merchandiseName,
        out_trade_no: orderNu,
        total_fee: total_fee,
        openid: app.globalData.openid
      },
      success(resp) {
        if (resp.data.success) {
          wx.requestPayment({
            'timeStamp': resp.data.data.timeStamp,
            'nonceStr': resp.data.data.nonceStr,
            'package': resp.data.data.package,
            'signType': 'MD5',
            'paySign': resp.data.sign,
            'success': function (res) {
              console.log(res);

              var options = {};
              options.course = JSON.stringify(that.data.course);
              that.onLoad(options);
            },
            'fail': function (res) {
              console.log(res);
            }
          });
        } else {
          wx.showToast({
            icon: 'none',
            title: resp.data.errorMsg,
          })
        }
      },
      fail(resp) {
        console.log(resp);
      }
    });
  },

  weixpay(e) {
    this.showPayDlg(this.data.order.id);
  },
})