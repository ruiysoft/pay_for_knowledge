// pages/order/index.js

const app = getApp();

Page({
  /**
   * 页面的初始数据
   */
  data: {
    tabs: ["全部", "待付款", "待发货", "已完成", "已退款"],
    activeIndex: 0,
    sliderOffset: 0,
    sliderLeft: 0,

    orderList1: [],
    orderList2: [],
    orderList3: [],
    orderList4: [],
    orderList5: [],
  },

  tabClick: function (e) {
    this.setData({
      sliderOffset: e.currentTarget.offsetLeft,
      activeIndex: e.currentTarget.id
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;
    
    wx.request({
      url: app.globalData.serverUrl + '/order/queryByUserId',
      data: { 
        id: app.globalData.employeeId,
        orderState: null 
      },
      success(resp) {
        if (resp.data.success) {
          that.setData({
            orderList1: resp.data.data
          });
        }
      },
      fail(e) {
        wx.showToast({
          icon: 'none',
          title: e.errMsg,
        });
      }
    });

    wx.request({
      url: app.globalData.serverUrl + '/order/queryByUserId',
      data: {
        id: app.globalData.employeeId,
        orderState: 'courseStatus_01'
      },
      success(resp) {
        if (resp.data.success) {
          that.setData({
            orderList2: resp.data.data
          });
        }
      },
      fail(e) {
        wx.showToast({
          icon: 'none',
          title: e.errMsg,
        });
      }
    });

    wx.request({
      url: app.globalData.serverUrl + '/order/queryByUserId',
      data: {
        id: app.globalData.employeeId,
        orderState: 'courseStatus_02'
      },
      success(resp) {
        if (resp.data.success) {
          that.setData({
            orderList3: resp.data.data
          });
        }
      },
      fail(e) {
        wx.showToast({
          icon: 'none',
          title: e.errMsg,
        });
      }
    });

    wx.request({
      url: app.globalData.serverUrl + '/order/queryByUserId',
      data: {
        id: app.globalData.employeeId,
        orderState: 'courseStatus_05'
      },
      success(resp) {
        if (resp.data.success) {
          that.setData({
            orderList4: resp.data.data
          });
        }
      },
      fail(e) {
        wx.showToast({
          icon: 'none',
          title: e.errMsg,
        });
      }
    });

    wx.request({
      url: app.globalData.serverUrl + '/order/queryByUserId',
      data: {
        id: app.globalData.employeeId,
        orderState: 'courseStatus_04'
      },
      success(resp) {
        if (resp.data.success) {
          that.setData({
            orderList5: resp.data.data
          });
        }
      },
      fail(e) {
        wx.showToast({
          icon: 'none',
          title: e.errMsg,
        });
      }
    });

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})