//app.js
App({
  onLaunch: function () {
    // 展示本地存储能力
    // var logs = wx.getStorageSync('logs') || []
    // logs.unshift(Date.now())
    // wx.setStorageSync('logs', logs);

    /*
    let that = this;
    wx.getStorage({
      key: 'openid',
      success: function (res) {
        console.log(res.data)
        // that.queryEmployeeByOpenId(res.data);
      },
      fail: function (res) {
        console.log(res);
        // 登录
        wx.login({
          success: res => {
            // 发送 res.code 到后台换取 openId, sessionKey, unionId
            wx.request({
              // method: "POST",
              url: that.globalData.serverUrl + '/user/get_openId_sessionKey_unionId',
              data: {
                code: res.code
              },
              success: function (resp) {
                console.log(resp);
                if (resp.data.success) {
                  let obj = JSON.parse(resp.data.data);

                  that.globalData.session_key = obj.session_key;
                  that.globalData.openid = obj.openid;
                }
              },
              fail: function (error) {
                console.error(error);
              }
            });
          }
        });
      }
    });
    */

    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            lang: "zh_CN",
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo;

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          });
        }
      }
    })
  },
  globalData: {
    userInfo: null,
    serverUrl: "https://www.yogapro.cn/xiaocx",
    // serverUrl: "http://192.168.1.100:8080",
    // serverUrl: "http://localhost:8080",
    session_key: null,
    openid: null,
    employeeId: null,
    isNewUser: false,
  }
})